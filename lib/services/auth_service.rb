require 'active_support'
require 'active_support/core_ext'
require 'rest-client'
require 'config_service'

class AuthService
  class << self
    def load_config
      @@config ||= ConfigService.load_config('auth_keys.yml').send(ConfigService.environment)
    end

    def set_config(options = {})
      @@config['url']             = options['url']
      @@config['consumer_key']    = options['consumer_key']
      @@config['consumer_secret'] = options['consumer_secret']

      @@config
    end

    def oauth_token(options = {})
      url     = options['url']              || @@config['url']
      key     = options['consumer_key']     || @@config['consumer_key']
      secret  = options['consumer_secret']  || @@config['consumer_secret']
      response = RestClient.post(url,
                                "client_id=#{key}&client_secret=#{secret}&grant_type=client_credentials",
                                 {'Content-Type' => 'application/x-www-form-urlencoded', 'grant_type' => 'client_credentials'} )
      JSON.parse(response)
    end

  end

  load_config
end
