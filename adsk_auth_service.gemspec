Gem::Specification.new do |s|
  s.name        = 'adsk_auth_service'
  s.version     = '1.1.1'
  s.summary     = "Autodesk second phase token retrieval"
  s.description = "A gem for Autodesk 2-phase authentication service."
  s.authors     = ['Linh Chau']
  s.email       = 'chauhonglinh@gmail.com'
  s.files       = [
                    './Gemfile', './adsk_auth_service.gemspec',
                    'lib/adsk_auth_service.rb',
                    'lib/services/auth_service.rb'
                  ]
  s.homepage    = 'https://github.com/linhchauatl/adsk_auth_service'
  s.license     = 'MIT'
  s.add_runtime_dependency 'config_service'
  s.add_runtime_dependency 'rest-client'

  s.add_development_dependency 'rspec', '~> 3.1'
end
